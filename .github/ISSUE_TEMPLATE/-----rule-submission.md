---
name: 规则提交/Rule submission
about: 规则提交/Rule submission
title: ''
labels: ''
assignees: ''

---

<!--template:reg-->
<!--label-->
<!--Please do not delete the above two notes. Just log in to your account and submit [issue]-->
<!--上面2个注释请勿删除，登录您的帐号提交【issue】即可-->


**Update Log/更新日志**

<!--请删除本行并填写更新日志，没有日志请删除本行-->

**Data/数据**

```json
<!--请删除本行并粘贴数据部分-->
```
