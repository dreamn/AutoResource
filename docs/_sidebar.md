<div style="background-color:#fafbff">
<div style="background-color:#f5f6fa;height:2.3em;font-size:1.2em;vertical-align: middle;padding:3px 0 3px 12px;">

- [<font size="4">**教程说明**</font>](教程说明.md)

</div>
<details open="open">
<summary style="background-color:#f5f6fa;line-height:2.3em;font-size:1.2em;text-align: justify;padding-left:8px;"><b>基础篇</b><span style="font-size:0.8rem;color:#95969a">（点击展开/收起）</span></summary>
<div style="margin-left:30px;">

- [开始前的准备](1.开始前的准备.md)
- [权限设置](2.权限设置.md)
- [资产设置与同步](资产设置与同步.md)
- [资产映射](资产映射.md)
- [分类设置与同步](分类设置与同步.md)
- [插件设置](插件设置.md)

</div>
</details>
<details open="open">
<summary style="background-color:#f5f6fa;line-height:2.3em;font-size:1.2em;text-align: justify;padding-left:8px;"><b>使用篇</b><span style="font-size:0.8rem;color:#95969a">（点击展开/收起）</span></summary>
<div style="margin-left:30px">

- [规则导入](规则导入.md)
- [规则书写](规则书写.md)
- [对账功能](对账功能.md)
- [报销功能](报销功能.md)
- [模板记账](模板记账.md)

</div>
</details>
<details open="open">
<summary style="background-color:#f5f6fa;line-height:2.3em;font-size:1.2em;text-align: justify;padding-left:8px;"><b>进阶篇</b><span style="font-size:0.8rem;color:#95969a">（点击展开/收起）</span></summary>
<div style="margin-left:30px">

- [正则表达式](正则表达式.md)
- [JS(JavaScript)](JS(JavaScript).md)
- [日志](日志.md)

</div>
</details>
<details open="open">
<summary style="background-color:#f5f6fa;line-height:2.3em;font-size:1.2em;text-align: justify;padding-left:8px;"><b>问题篇</b><span style="font-size:0.8rem;color:#95969a">（点击展开/收起）</span></summary>
<div style="margin-left:30px">

- [常见问题之 参数错误](常见问题之参数错误.md)
- [常见问题之 为什么不记账](常见问题之为什么不记账.md)
- [常见问题之 找不到文件](常见问题之找不到文件.md)
- [崩溃问题](崩溃问题.md)
- [其他问题](其他问题.md)
- [意见反馈](意见反馈.md)

</div>
</details>
<details open="open">
<summary style="background-color:#f5f6fa;line-height:2.3em;font-size:1.2em;text-align: justify;padding-left:8px;"><b>其他</b><span style="font-size:0.8rem;color:#95969a">（点击展开/收起）</span></summary>
<div style="margin-left:30px">

- [贡献代码](Contribution.md)
- [更新日志](ChangeLog.md)
- [开源协议](LICENSE.md)
- [相关资料](参考资料.md)

</div>
</details>
</div>

