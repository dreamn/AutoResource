# 贡献代码

> 需要您会Java、会使用Git进行分支合并

## 一、Fork自动记账

![2021031718203116159764311615976431621lBZHdfv2Q8Vq](https://pic.dreamn.cn/uPic/2021031718203816159764381615976438549fMnWa22021031718203116159764311615976431621lBZHdfv2Q8Vq.png)

## 二、将代码Clone到本地

```shell
git clone https://github.com/dreamncn/Qianji_auto.git
```

!> 注意：此处的地址是您的仓库地址，并不是自动记账的仓库地址

## 三、使用AndroidStudio打开项目文件

> 等待自动编译完成，修改代码，并提交到您自己的仓库中

## 四、从您的仓库发起合并请求（Pull Requests）

![image-20210317182636559](https://pic.dreamn.cn/uPic/2021031718263616159767961615976796973FZvUprimage-20210317182636559.png)

![image-20210317182700971](https://pic.dreamn.cn/uPic/2021031718270116159768211615976821400ln2xJPimage-20210317182700971.png)

!> **发起合并请求请注意：**简明扼要地说明你修改了什么，添加了什么功能。

## 五、等待审核通过，合并到主分支



